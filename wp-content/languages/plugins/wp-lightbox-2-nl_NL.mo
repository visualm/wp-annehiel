��            )         �     �     �  %   �     �  T        \     o  p   x     �       7     0   C  *   t     �  l   �          &     /     B  *   Y     �     �     �  �   �  �   �     |     �  -   �  
   �     �  "  �     	     	     $	     D	  d   c	     �	     �	  }   �	     ^
  
   {
  5   �
  ?   �
  ,   �
     )  t   :     �     �     �     �  B        F     R     j    x  �   ~     j     �  -   �     �     �                        	                                                                                                            
     of  &laquo; Previous Animation duration (in milliseconds)  Auto-lightbox image links Browse images with your keyboard: Arrows or P(revious)/N(ext) and X/C/ESC for close. Cheatin&#8217; uh? Download Enable lightbox in comments (disables <a href="http://codex.wordpress.org/Nofollow">the nofollow attribute!</a>) Help text (default: none)  Image  Keep a distance between the image and the screen edges. Let the plugin add necessary html to image links Minimum margin to screen edge (default: 0) Next &raquo; Note: this will disable the nofollow-attribute of comment links, that otherwise interfere with the lightbox. Save Changes Settings Show download link Show image info on top Shrink large images to fit smaller screens Syed Balkhi Target for download link: WP Lightbox 2 WP Lightbox 2 is awesome tool for adding responsive lightbox (overlay) effect for images and also create lightbox for photo albums/galleries on your WordPress blog. WordPress Lightbox is one of the most useful plugins for your website. _blank: open the image in a new window or tabr
_self: open the image in the same frame as it was clicked (default)r
_parent: open the image in the parent framesetr
_top: open the image in the full body of the window close image gallery http://syedbalkhi.com http://wpdevart.com/wordpress-lightbox-plugin next image previous image PO-Revision-Date: 2020-07-02 14:20:15+0000
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: GlotPress/3.0.0-alpha.2
Language: nl
Project-Id-Version: Plugins - WP Lightbox 2 - Development (trunk)
 van &laquo; Vorige Animatieduur (in milliseconden) Auto-lightbox-afbeeldingslinks Blader door afbeeldingen met je toetsenbord: pijlen of P (revious)/N (ext) en X/C/ESC om te sluiten. Valsspelen he? Download Schakel lightbox in opmerkingen in (schakelt <a href="http://codex.wordpress.org/Nofollow"> het nofollow-attribuut uit! </a>) Help-tekst (standaard: geen) Afbeelding Houd een afstand tussen het beeld en de schermranden. Laat de plugin de benodigde html toevoegen aan afbeeldingslinks Minimale marge tot schermrand (standaard: 0) Volgende &raquo; Opmerking: hierdoor wordt het nofollow-kenmerk van reactieaarlinks uitgeschakeld, die anders de lightbox verwinkeln. Bewaar wijzigingen Instellingen Downloadlink weergeven Beeldinfo bovenaan weergeven Grote afbeeldingen verkleinen zodat ze op kleinere schermen passen Syed Balkhi Doel voor downloadlink: WP Lightbox 2 WP Lightbox 2 is een geweldige tool voor het toevoegen van een responsief lightbox (overlay) effect voor afbeeldingen en maak ook een lightbox voor afbeeldingalbums/galerijen op je WordPress-blog. WordPress Lightbox is een van de handigste plugins voor je site. _blank: open de afbeelding in een nieuw venster of tab
_self: open de afbeelding in dezelfde pagina als hij was aangeklikt (standaard)r
_parent: open de afbeelding in het hoofd framesetr
_top: open de afbeelding in het volledige scherm afbeeldingsgalerij sluiten http://syedbalkhi.com http://wpdevart.com/wordpress-lightbox-plugin volgende afbeelding vorige afbeelding 