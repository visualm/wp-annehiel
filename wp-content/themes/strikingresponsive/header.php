<?php
/**
 * The Header for our theme
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
?><!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) | !(IE 8) ]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width">
	<title><?php wp_title( '|', true, 'right' ); ?></title>
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
    <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/bootstrap.css" />
	<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/slick.css" />

	<!--[if lt IE 9]>
	<script src="<?php echo get_template_directory_uri(); ?>/js/html5.js"></script>
	<![endif]-->

    <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/jquery.bxslider.css">
	<link rel="stylesheet" id="theme-skin-css" href="<?php bloginfo('template_url'); ?>/js/skin.css" type="text/css" media="all">
 	<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/jquery.min.js"></script>
	<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/jquery.bxslider.js"></script>
	<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/jquery.easing.1.3.js"></script>
	<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/slick.min.js"></script>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-15253736-16"></script>
<script>
window.dataLayer = window.dataLayer || [];
function gtag(){dataLayer.push(arguments);}
gtag('js', new Date());

gtag('config', 'UA-15253736-16');
</script>

	<?php wp_head(); ?>

    <?php if(is_page(1334)) { ?>
	    <link rel="stylesheet" id="theme-style-css" href="<?php bloginfo('template_url'); ?>/js/screen_complex.css" type="text/css" media="all">
		<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/jquery-1.js"></script>
		<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/jquery_003.js"></script>
		<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/jquery_006.js"></script>
		<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/jquery_008.js"></script>
		<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/jquery_007.js"></script>

		<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/custom.js"></script>
		<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/kwicksSliderInit.js"></script>
	  	<script src='<?php bloginfo('template_url'); ?>/js/jquery.kwicks.js' type='text/javascript'></script>
	<?php } ?>
<!-- <script type="text/javascript">
Cufon.replace("h1,h2,h4,h5", {fontFamily : "Daniel"});
</script> -->


</head>

<body <?php body_class(); ?>>
<header id="header">
	<div class="inner col-sm-9 col-xs-12 container abc">
    <div id="certheader">
		<?php if ( get_field( 'logo_fsc', 'option' ) ) : ?>
			<a target="_blank" href="<?php the_field('logo_fsc_link', 'options'); ?>"><img style="margin: 8px 10px 0px 0px;" src="<?php the_field('logo_fsc', 'options'); ?>" border="0" alt="Link to this page"></a>
		<?php endif; ?>
		<?php if ( get_field( 'logo_avih', 'option' ) ) : ?>
			<a target="_blank" href="<?php the_field('logo_avih_link', 'options') ?>"><img src="<?php the_field('logo_avih', 'options'); ?>" border="0" alt="Link to this page"></a>
		<?php endif; ?>

		<?php if ( get_field( 'logo_er', 'option' ) ) : ?>
			<a target="_blank"  href="<?php the_field('logo_er_link', 'options'); ?>"><img style="margin:0px 0px 2px 10px;"  src="<?php the_field('logo_er', 'options'); ?>" border="0" alt="Link to this page"></a>
		<?php endif; ?>
	</div><!--certheader-->

<div id="logo">
	<a href="<?php bloginfo('url'); ?>"><img class="ie_png" src="<?php the_field('logo_annehiel', 'options'); ?>" alt="<?php echo get_field('logo_annehiel_alt', 'options'); ?>"/></a>
 </div>
 <?php wp_nav_menu( array( 'theme_location' => 'primary','menu_id'=>'menu-menu', 'menu_class' => 'menu','container' => 'div','container_id' => 'navigation' ) ); ?>
  <div class="mobile_menu"><?php  wp_nav_menu( array(

		'menu' => 'mobile_340',

        'depth'      => 3,

        'container'  => false,
		'items_wrap'     => '<form><select name="sel" class="mob_340" onchange="if (this.value) window.location.href=this.value">%3$s</select></form>',
         'before' => '<option>',
		 'after'  => '</option>',
        'menu_class'      => 'nav  nav-pills nav-main dropdown',
		'menu_id'         => 'mainMenu',
        'fallback_cb' => 'wp_page_menu',

        //Process nav menu using our custom nav walker

        'walker' => new twitter_bootstrap_nav_walker())

    );
?></div>
</div><!--inner-->
</header>
<script type='text/javascript'>
$(document).ready(function(){
	$("ul.menu li").hover(
    function () {
    $(this).addClass("hover");
  },
	function () {
    $(this).removeClass("hover");
  }
	    );
	$("li.hover ul").toggle('slow');

});
</script>
