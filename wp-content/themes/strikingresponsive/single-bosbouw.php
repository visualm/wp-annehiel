<?php get_header(); ?>

<?php if ( get_field( 'hero_fallback', 'option' ) ) : ?>
	<?php $background = get_field( 'hero_fallback', 'option' ); ?>
<?php endif ?>

<?php if ( get_field( 'header_afbeelding', 'option' ) ) : ?>
	<?php $background = get_field('header_afbeelding', 'options'); ?>
<?php endif ?>

<div id="feature" style="background-image: url(<?php echo $background; ?>)">
	<div class="top_shadow"></div>
		<div class="inner col-sm-9 col-xs-12 container">
			<div class="page_titles"> <?php  the_title(); ?> </div>
		</div><!--inner col-sm-9 col-xs-12-->
	<div class="bottom_shadow"></div>
</div><!-- feature -->

<div class="page_top"></div><!--page_top-->
<!-- Notification block -->
<?php if ( get_field( 'mededeling_actief', 'option' ) == 1 ) : ?>
	<div class="container">
		<div class="vacation-notification__wrapper">
			<?php if ( get_field( 'mededeling_titel', 'option' ) ) : ?>
				<div class="vacation-notification__title">
					<?php the_field( 'mededeling_titel', 'option' ); ?>
				</div>
			<?php endif; ?>
			<?php if ( get_field( 'mededeling_tekst', 'option' ) ) : ?>
				<div class="vacation-notification__text">
					<?php the_field( 'mededeling_tekst', 'option' ); ?>
				</div>
			<?php endif; ?>
		</div>
	</div>
<?php endif; ?>
<!-- Notification block -->

<div class="clear"></div>

<?php //$slidercount = 1;?>
<?php if ( have_rows( 'bosbouw' ) ): ?>
	<?php while ( have_rows( 'bosbouw' ) ) : the_row(); ?>
		<?php if ( get_row_layout() == 'techniek' ) : ?>

					<div class="page_titles">.</div>

			<div class="inner col-sm-9 col-xs-12 col-md-9 col-lg-9 container">
				<div class="post_images col-xs-12 col-md-6 col-sm-6 col-lg-6">
					<div id="slider1" class="slider<?php //echo $slidercount;?>">
						<?php if ( have_rows( 'single_image' ) ) : ?>
							<?php while ( have_rows( 'single_image' ) ) : the_row(); ?>
								<?php if ( get_sub_field( 'single_page_image' ) ) { ?>
									<img src="<?php the_sub_field( 'single_page_image' ); ?>" />
								<?php } ?>
							<?php endwhile; ?>
						<?php else : ?>
							<?php // no rows found ?>
						<?php endif; ?>
						<?php //$slidercount++;?>
					</div><!-- slider -->
				</div><!-- post-images -->

				<div class="post_content col-xs-12 col-md-6 col-sm-6 col-lg-6">
					<div class="page_title"><?php the_sub_field( 'techniek_titel' ); ?></div>
					<?php the_sub_field( 'techniek_tekst' ); ?>
				</div><!-- post_content -->
			</div><!-- inner -->

			<div class="clear"></div>

			<div class="inner video_container col-sm-9 col-xs-12 col-md-9 col-lg-9 container">
				<?php if ( have_rows( 'select_single_page_video' ) ) : ?>
					<?php while ( have_rows( 'select_single_page_video' ) ) : the_row(); ?>
						<div class="post_video col-xs-12 col-md-6 col-sm-6 col-lg-6">
							<div class="videoWrapper">
								<?php the_sub_field( 'single_video' ); ?>
		      				</div>
		      			</div><!-- post video -->
					<?php endwhile; ?>
				<?php else : ?>
					<?php // no rows found ?>
				<?php endif; ?>
			</div><!-- inner -->
			<hr>
		<?php endif; ?>
	<?php endwhile; ?>
<?php else: ?>
	<?php // no layouts found ?>
<?php endif; ?>

<div class="clear"></div>

<div class="page_bottom"></div>

<script type="text/javascript">
	$(document).ready(function(){

		$( ".slider" ).hide();
		$( ".videoWrapper" ).hide();

		var st = setTimeout(function () {
			$( ".slider" ).show();
			$( ".videoWrapper" ).show();

			$('.slider').slick({
				dots: true,
				arrows: true,
				autoplay: true,
				autoplaySpeed: 2000,
			});
		}, 500);
	});
</script>

<?php get_footer();?>
