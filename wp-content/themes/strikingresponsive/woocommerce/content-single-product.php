<?php
/**
 * The template for displaying product content in the single-product.php template
 *
 * Override this template by copying it to yourtheme/woocommerce/content-single-product.php
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
?>

<?php
	/**
	 * woocommerce_before_single_product hook
	 *
	 * @hooked wc_print_notices - 10
	 */
	 do_action( 'woocommerce_before_single_product' );

	 if ( post_password_required() ) {
	 	echo get_the_password_form();
	 	return;
	 }
?>

<div itemscope itemtype="<?php echo woocommerce_get_product_schema(); ?>" id="product-<?php the_ID(); ?>" <?php post_class(); ?>>
	<?php
		/**
		 * woocommerce_before_single_product_summary hook
		 *
		 * @hooked woocommerce_show_product_sale_flash - 10
		 * @hooked woocommerce_show_product_images - 20
		 */
		 function custom_show_product_images() {
		   echo 'jajajaj';
		 }
		do_action( 'woocommerce_before_single_product_summary' );
	?>

	<div class="summary entry-summary">
		<?php
			/**
			 * woocommerce_single_product_summary hook
			 *
			 * @hooked woocommerce_template_single_title - 5
			 * @hooked woocommerce_template_single_rating - 10
			 * @hooked woocommerce_template_single_price - 10
			 * @hooked woocommerce_template_single_excerpt - 20
			 * @hooked woocommerce_template_single_add_to_cart - 30
			 * @hooked woocommerce_template_single_meta - 40
			 * @hooked woocommerce_template_single_sharing - 50
			 */
			woocommerce_template_single_title();
			woocommerce_template_single_excerpt();

			$term_list = wp_get_post_terms($post->ID, 'product_cat', array("fields" => "all"));
			if (!empty($term_list)) {
				foreach ($term_list as $term) {
					addOrderbtn($term->slug);
				}
			}
			woocommerce_template_single_meta();
		?>

	</div><!-- .summary -->
	<div class="clear"></div>
	<?php
			global $post;
			 $terms = get_the_terms( $post->ID, 'product_cat' );
			 $nterms = get_the_terms( $post->ID, 'product_tag'  );
		 foreach ($terms  as $term  ) {
			 $product_cat_id = $term->term_id;
			 $product_cat_name = $term->name;
			 if ($term->name == 'Tuinmeubilair') : ?>
			 	<div style="margin: 0 0 25px 0;">
					<p>Bestel uw tuinmeubel via de onderstaande knop. Vermeld het product wat u graag zou willen, de afmetingen en uw naam, adres, woonplaats en telefoonnummer.</p>
					<a href="mailto:info@annehiel.nl" class="left read_more">Plaats een bestelling</a>
				</div>
		 <?php endif;
		 }
	?>
	<?php
		/** 
		 * woocommerce_after_single_product_summary hook
		 *
		 * @hooked woocommerce_output_product_data_tabs - 10
		 * @hooked woocommerce_output_related_products - 20
		 */
		do_action( 'woocommerce_after_single_product_summary' );
	?>
	<meta itemprop="url" content="<?php the_permalink(); ?>" />

</div><!-- #product-<?php the_ID(); ?> -->
<?php do_action( 'woocommerce_after_single_product' ); ?>
