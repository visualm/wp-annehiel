<?php
/**
 * The Template for displaying all single products.
 *
 * Override this template by copying it to yourtheme/woocommerce/single-product.php
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

get_header( 'shop' ); ?>

<?php if ( get_field( 'hero_fallback', 'option' ) ) : ?>
	<?php $background = get_field( 'hero_fallback', 'option' ); ?>
<?php endif ?>

<?php if ( get_field( 'header_afbeelding', 'option' ) ) : ?>
	<?php $background = get_field('header_afbeelding', 'options'); ?>
<?php endif ?>

<div id="feature" style="background-image: url(<?php echo $background; ?>)">
	<div class="top_shadow"></div>
       <div class="inner col-sm-9 col-xs-12 blog_set container">

     <h1><?php the_title(); ?></h1>


        </div><!--inner col-sm-9 col-xs-12-->
       <div class="bottom_shadow"></div>
   </div><!--feature-->
   <div class="page_top"> </div>
   <!-- Notification block -->
   <?php if ( get_field( 'mededeling_actief', 'option' ) == 1 ) : ?>
	   <div class="container">
		   <div class="vacation-notification__wrapper">
			   <?php if ( get_field( 'mededeling_titel', 'option' ) ) : ?>
				   <div class="vacation-notification__title">
					   <?php the_field( 'mededeling_titel', 'option' ); ?>
				   </div>
			   <?php endif; ?>
			   <?php if ( get_field( 'mededeling_tekst', 'option' ) ) : ?>
				   <div class="vacation-notification__text">
					   <?php the_field( 'mededeling_tekst', 'option' ); ?>
				   </div>
			   <?php endif; ?>
		   </div>
	   </div>
   <?php endif; ?>
   <!-- Notification block -->

    <div class="inner col-sm-9 col-xs-12 container">
	<?php
		/**
		 * woocommerce_before_main_content hook
		 *
		 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
		 * @hooked woocommerce_breadcrumb - 20
		 */
		do_action( 'woocommerce_before_main_content' );
	?>
		<?php while ( have_posts() ) : the_post(); ?>

			<?php wc_get_template_part( 'content', 'single-product' ); ?>

		<?php endwhile; // end of the loop. ?>

	<?php
		/**
		 * woocommerce_after_main_content hook
		 *
		 * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
		 */
		do_action( 'woocommerce_after_main_content' );
	?>

	<?php
		/**
		 * woocommerce_sidebar hook
		 *
		 * @hooked woocommerce_get_sidebar - 10
		 */
		//do_action( 'woocommerce_sidebar' );
	?>
    <div class="clear"></div>
</div>
<?php get_footer( 'shop' ); ?>
