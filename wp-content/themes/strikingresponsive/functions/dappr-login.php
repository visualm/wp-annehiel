<?php
add_action('login_head', 'dappr_login');

function dappr_login() {
    echo '<link href="https://fonts.googleapis.com/css?family=Poppins&display=swap" rel="stylesheet">';
    echo '<style>
              body.login {
              background: #1d2227 url("' . get_site_url() . '/wp-content/themes/strikingresponsive/images/login-bg.png") no-repeat 100% 100%;
              background-size: 100% 100%;
              font-family: "Poppins", sans-serif;
              color: #fff;
            }
						div#login {
							position: relative;
							z-index: 2;
						}
            body.login #login h1 a {
              background: url("' . get_site_url() . '/wp-content/themes/strikingresponsive/images/annehiel-dappr.png") no-repeat scroll center top transparent;
              height: 100px;
              width: 100%;
              background-size: 100%;
            }
            form#loginform {
              -webkit-box-shadow: 0 20px 75px rgba(0,0,0,.5);
              box-shadow: 0 20px 75px rgba(0,0,0,.5);
              background-color: #1d2227;
              border: 0;
            }
            .login #login_error, .login .message, .login .success {
              border-left: 4px solid #ED2224;
              -webkit-box-shadow: 0 20px 75px rgba(0,0,0,.5);
              box-shadow: 0 20px 75px rgba(0,0,0,.5);
              background-color: #1d2227;
            }
            .login label {
                color: #fff;
            }
            .login #backtoblog a, .login #nav a {
                color: #fff;
            }
						.dappr_login {
							background: transparent url("' . get_site_url() . '/wp-content/themes/strikingresponsive/images/dappr-big.png") no-repeat 50%;
    					background-size: contain;
							width: 100%;
							height: 100%;
							position: absolute;
							z-index: 1;
						}
        </style>';

				echo '<div class="dappr_login"></div>';
}
?>
