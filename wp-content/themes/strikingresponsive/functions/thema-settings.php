<?php

// Add ACF options page
if( function_exists('acf_add_options_page') ) {
	$option_page = acf_add_options_page(array(
		'page_title' 	=> 'Thema Settings',
		'menu_title' 	=> 'Thema Settings',
		'menu_slug' 	=> 'thema-settings',
		'capability' 	=> 'edit_posts',
		'redirect' 	=> false
	));
}
