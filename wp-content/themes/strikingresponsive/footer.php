<?php
/**
 * The template for displaying the footer
 *
 * Contains footer content and the closing of the #main and #page div elements.
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
?>


		</div><!-- #main -->




		<footer id="footer">
          <div class="footer_shadow"></div>
            <div class="inner col-sm-9 col-xs-12 container">
               <div class="col-sm-3">
                <?php dynamic_sidebar('footer-1'); ?>
               </div>
               <div class="col-sm-3">
               <?php dynamic_sidebar('footer-2'); ?>
               </div>
               <div class="col-sm-3">
               <?php dynamic_sidebar('footer-3'); ?>
               </div>
               <div class="col-sm-3">
               <?php dynamic_sidebar('footer-4'); ?>
               </div>
            <div class="clear"></div>
            </div><!--inner col-sm-9 col-xs-12-->

            <div id="footer_bottom">
               <div class="inner col-sm-9 col-xs-12 container">
                  <div id="copyright">Copyright © 2012-<?php echo date("Y"); ?> Annehiel rondhoutverwerking | <a href="http://www.annehiel.nl/algemenevoorwaarden-v3.pdf" target="_blank">Algemene voorwaarden</a></div>
               </div><!--inner col-sm-9 col-xs-12-->
            </div>

		</footer><!-- #colophon -->
	</div><!-- #page -->

	<?php wp_footer(); ?>
</body>
</html>
