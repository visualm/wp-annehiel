<?php
/**
 * The template for displaying Category pages
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header(); ?>

<?php if ( get_field( 'hero_fallback', 'option' ) ) : ?>
	<?php $background = get_field( 'hero_fallback', 'option' ); ?>
<?php endif ?>

<?php if ( get_field( 'header_afbeelding', 'option' ) ) : ?>
	<?php $background = get_field('header_afbeelding', 'options'); ?>
<?php endif ?>

<div id="feature" style="background-image: url(<?php echo $background; ?>)">
	<div class="top_shadow"></div>
       <div class="inner col-sm-9 col-xs-12 blog_set container">

         <h1><?php printf( __( '%s', 'twentyfourteen' ), single_cat_title( '', false ) ); ?></h1>

        </div><!--inner col-sm-9 col-xs-12-->
       <div class="bottom_shadow"></div>
   </div>
     <div class="page_top"> </div>
	 <!-- Notification block -->
	 <?php if ( get_field( 'mededeling_actief', 'option' ) == 1 ) : ?>
		 <div class="container">
			 <div class="vacation-notification__wrapper">
				 <?php if ( get_field( 'mededeling_titel', 'option' ) ) : ?>
					 <div class="vacation-notification__title">
						 <?php the_field( 'mededeling_titel', 'option' ); ?>
					 </div>
				 <?php endif; ?>
				 <?php if ( get_field( 'mededeling_tekst', 'option' ) ) : ?>
					 <div class="vacation-notification__text">
						 <?php the_field( 'mededeling_tekst', 'option' ); ?>
					 </div>
				 <?php endif; ?>
			 </div>
		 </div>
	 <?php endif; ?>
	 <!-- Notification block -->

	<div class="inner col-sm-9 col-xs-12 container">
    <?php if ( have_posts() ) : ?>

			<header class="archive-header">
				<h2 class="archive-title"><?php printf( __( '%s', 'twentyfourteen' ), single_cat_title( '', false ) ); ?></h2>

				<?php

					// Show an optional term description.
					$term_description = term_description();
					if ( ! empty( $term_description ) ) :
						printf( '<div class="taxonomy-description">%s</div>', $term_description );
					endif;
				?>
			</header>

    <div class="woo_prroduct_outer">
  <div class="static_result">
    <div class="woo_prroductdate col-sm-2 formobflot ">Datum</div>
    <div class="woo_prroducttime col-sm-2 formobflot ">Tijd</div>
    <div class="woo_prroduct_des col-sm-6 formobflot">Onderdeel</div>
    <div class="woo_prroductlink col-sm-2 formobflot"></div>

  </div>


</div>




		<!-- .archive-header -->

			<?php
					// Start the Loop.
					while ( have_posts() ) : the_post(); ?>
                    <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<?php twentyfourteen_post_thumbnail(); ?>




     <div class="woo_prroduct_outer">
  <div class="static_result">
   <?php    $postid = get_the_ID();     ?>
    <div class="bmob_static_data">Datum</div>
    <div class="woo_prroductdate col-sm-2 not_bold formobflot"><?php echo get_post_meta($post->ID,'my_meta_box_text_date',true); ?></div>
    <div class="bmob_static_data">Tijd</div>
    <div class="woo_prroducttime col-sm-2 not_bold formobflot"><?php echo get_post_meta($post->ID,'my_meta_box_text_time',true); ?></div>
    <div class="bmob_static_data">Onderdeel</div>
    <div class="woo_prroduct_des col-sm-6 not_bold textleft formobflot"><?php  the_title();?><?php /*?><?php echo get_post_meta($post->ID,'my_meta_box_text',true); ?><?php */?></div>
    <div class="bmob_static_data"></div>
    <div class="woo_prroductlink col-sm-2 not_bold formobflot"><a href="<?php the_permalink(); ?>">Lees verder</a></div>
  <div class="clear"></div>
  </div>


</div>

	<?php /*?><header class="entry-header">
		<?php if ( in_array( 'category', get_object_taxonomies( get_post_type() ) ) && twentyfourteen_categorized_blog() ) : ?>
		<div class="entry-meta">
			<span class="cat-links"><?php echo get_the_category_list( _x( ', ', 'Used between list items, there is a space after the comma.', 'twentyfourteen' ) ); ?></span>
		</div>
		<?php
			endif;

			if ( is_single() ) :
				the_title( '<h1 class="entry-title">', '</h1>' );
			else :
				the_title( '<h1 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h1>' );
			endif;
		?>

		<div class="entry-meta">
			<?php
				if ( 'post' == get_post_type() )
					twentyfourteen_posted_on();

				if ( ! post_password_required() && ( comments_open() || get_comments_number() ) ) :
			?>
			<span class="comments-link"><?php comments_popup_link( __( 'Leave a comment', 'twentyfourteen' ), __( '1 Comment', 'twentyfourteen' ), __( '% Comments', 'twentyfourteen' ) ); ?></span>
			<?php
				endif;

				edit_post_link( __( 'Edit', 'twentyfourteen' ), '<span class="edit-link">', '</span>' );
			?>
		</div><!-- .entry-meta -->
	</header><?php */?><!-- .entry-header -->

	<?php if ( is_search() ) : ?>
	<div class="entry-summary">
	<?php /*?>	<?php the_excerpt(); ?><?php */?>
	</div><!-- .entry-summary -->
	<?php else : ?>
	<?php /*?><div class="entry-content">
		<?php
			the_content( __( 'Continue reading <span class="meta-nav">&rarr;</span>', 'twentyfourteen' ) );
			wp_link_pages( array(
				'before'      => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'twentyfourteen' ) . '</span>',
				'after'       => '</div>',
				'link_before' => '<span>',
				'link_after'  => '</span>',
			) );
		?>
	</div><?php */?><!-- .entry-content -->
	<?php endif; ?>

	<?php the_tags( '<footer class="entry-meta"><span class="tag-links">', '', '</span></footer>' ); ?>
</article><!-- #post-## -->


<?php					//get_template_part( 'content', get_post_format() );

					endwhile;

					twentyfourteen_paging_nav();

				else :
					// If no content, include the "No posts found" template.
					get_template_part( 'content', 'none' );

				endif;
			?>
            </div>
             <div class="page_bottom"></div>
		<!-- #primary -->

<?php
//get_sidebar( 'content' );
//get_sidebar();
get_footer();
