<?php
/**
 * The template for displaying all pages
 * Template Name: Archive template
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other 'pages' on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header(); ?>
<?php if ( get_field( 'hero_fallback', 'option' ) ) : ?>
	<?php $background = get_field( 'hero_fallback', 'option' ); ?>
<?php endif ?>

<?php if ( get_field( 'header_afbeelding', 'option' ) ) : ?>
	<?php $background = get_field('header_afbeelding', 'options'); ?>
<?php endif ?>

<div id="feature" style="background-image: url(<?php echo $background; ?>)">
	<div class="top_shadow"></div>
       <div class="inner col-sm-9 col-xs-12">
       <?php if (function_exists('nivoslider4wp_show')) { nivoslider4wp_show(); } ?>
        </div><!--inner col-sm-9 col-xs-12-->
       <div class="bottom_shadow"></div>
   </div><!--feature-->
   <div class="page_top"> </div>
   <!-- Notification block -->
   <?php if ( get_field( 'mededeling_actief', 'option' ) == 1 ) : ?>
	   <div class="container">
		   <div class="vacation-notification__wrapper">
			   <?php if ( get_field( 'mededeling_titel', 'option' ) ) : ?>
				   <div class="vacation-notification__title">
					   <?php the_field( 'mededeling_titel', 'option' ); ?>
				   </div>
			   <?php endif; ?>
			   <?php if ( get_field( 'mededeling_tekst', 'option' ) ) : ?>
				   <div class="vacation-notification__text">
					   <?php the_field( 'mededeling_tekst', 'option' ); ?>
				   </div>
			   <?php endif; ?>
		   </div>
	   </div>
   <?php endif; ?>
   <!-- Notification block -->
    <div class="inner col-sm-9 col-xs-12">
    <?php if (function_exists('dimox_breadcrumbs')) dimox_breadcrumbs(); ?>
      <div class="content">

     <?php while ( have_posts() ) : the_post(); ?>
      <h2><?php the_title(); ?></h2>
	 <?php  //the_content();

     endwhile;  ?>
                <div class="woo_prroduct_outer">
  <div class="static_result">
    <div class="woo_prroductdate col-sm-2 ">Datum</div>
    <div class="woo_prroducttime col-sm-2 ">Tijd</div>
    <div class="woo_prroduct_des col-sm-6">Onderdeel</div>
    <div class="woo_prroductlink col-sm-2"></div>

  </div>


</div>
<?php echo do_shortcode('[product_category category="aanbiedingen" per_page="12" columns="4" orderby="date" order="desc"]');  ?>


       </div><!--content-->
    </div><!--inner col-sm-9 col-xs-12-->
    <div class="page_bottom"></div>
<?php
//get_sidebar();
get_footer();
