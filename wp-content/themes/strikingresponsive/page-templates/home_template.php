<?php
/**
* Template Name:Home Page
*
* @package WordPress
* @subpackage Twenty_Fourteen
* @since Twenty Fourteen 1.0
*/

get_header(); ?>
</script>
<?php while ( have_rows( 'slider_instellingen', 'option' ) ) : the_row(); ?>
	<script type="text/javascript">
	var slideShow = [];
		<?php if ( get_sub_field( 'automatisch_afspelen' ) == 1 ) : ?>
		slideShow['autoplay'] = true;
		<?php else : ?>
		slideShow['autoplay'] = false;
		<?php endif; ?>
		slideShow['pauseTime'] = 5000;
		slideShow['number'] = <?php the_sub_field( 'aantal_slides' ); ?>;
		slideShow['maxSize'] = 850;
		slideShow['duration'] = <?php the_sub_field( 'snelheid_slide' ); ?>;
		slideShow['easing'] = 'linear';
		slideShow['title'] = true;
		slideShow['title_speed'] = <?php the_sub_field( 'snelheid_titel' ); ?>;
		slideShow['title_opacity'] = 0.6;
		slideShow['detail'] = true;
		slideShow['detail_speed'] = <?php the_sub_field( 'snelheid_omschrijving' ); ?>;
		slideShow['detail_opacity'] = 0.6;
	</script>
<?php endwhile; ?>

<?php if ( get_field( 'hero_fallback', 'option' ) ) : ?>
	<?php $background = get_field( 'hero_fallback', 'option' ); ?>
<?php endif ?>

<?php if ( get_field( 'header_afbeelding', 'option' ) ) : ?>
	<?php $background = get_field('header_afbeelding', 'options'); ?>
<?php endif ?>

<div class="kwicks_slider" id="feature" style="background-image: url(<?php echo $background; ?>)">
	<div class="top_shadow"></div>
	<div class="inner col-sm-9 col-xs-12 container">
		<div class="kwicks_slider" id="">
			<div class="inner">
				<ul id="kwicks" class="kwicks-number-4 kwicks kwicks-horizontal kwicks-processed">

					<?php if ( have_rows( 'home_slider', 'options' ) ) : ?>
						<?php while ( have_rows( 'home_slider', 'options' ) ) : the_row(); ?>
							<li class="is-linkable" style="left:0px;width:240px;">
								<a href="<?php echo get_sub_field('slider_link', 'options'); ?>">
									<img  src="<?php echo get_sub_field('slider_afbeelding'); ?>" alt="<?php echo get_sub_field('slider_titel'); ?>" height="300" width="850">
									<div style="opacity: 0.6; display: block;" class="kwick_title">
										<?php echo get_sub_field('slider_titel'); ?>
									</div>
									<div style="width: 830px; opacity: 0; display: block;" class="kwick_detail">
										<h3><?php echo get_sub_field('slider_titel'); ?></h3>
										<div class="kwick_desc"><?php echo get_sub_field('slider_omschrijving'); ?></div>
									</div>
									<div class="kwick_shadow"> </div>
									<div class="kwick_frame_top"></div>
									<div class="kwick_frame"></div>
								</a>
							</li>
						<?php endwhile; ?>
			        <?php endif; ?>


					</ul>
					<!--<div id="kwicks_shadow"></div>-->
				</div>

			</div>

		</div><!--inner col-sm-9 col-xs-12-->
		<div class="bottom_shadow"></div>
	</div><!--feature-->
	<div class="page_top">


								</div><!--page_top-->

								<!-- Notification block -->
								<?php if ( get_field( 'mededeling_actief', 'option' ) == 1 ) : ?>
									<div class="container">
										<div class="vacation-notification__wrapper">
											<?php if ( get_field( 'mededeling_titel', 'option' ) ) : ?>
												<div class="vacation-notification__title">
													<?php the_field( 'mededeling_titel', 'option' ); ?>
												</div>
											<?php endif; ?>
											<?php if ( get_field( 'mededeling_tekst', 'option' ) ) : ?>
												<div class="vacation-notification__text">
													<?php the_field( 'mededeling_tekst', 'option' ); ?>
												</div>
											<?php endif; ?>
										</div>
									</div>
								<?php endif; ?>
								<!-- Notification block -->

								<div class="inner col-sm-9 col-xs-12 container">
									<div class="col-sm-8 col-xs-12">
										<div class="content for_facebook">
											<div class="facebook_social_icon">
												<a href="http://nl-nl.facebook.com/pages/Annehiel-Rondhoutverwerking-Hoogersmilde/264872203669466" target="_blank"><img src="<?php  bloginfo('template_url');?>/images/fb.png" /></a>
											</div>
											<?php while ( have_posts() ) : the_post();
											the_content(); endwhile;  ?>
										</div>
										<div class="content_bottum">
											<div class="col-md-7 col-xs-12">
												<h3>Nieuws/Activiteiten</h3>
												<?php $args = array('post_type' => 'post', 'posts_per_page'=>'1');
												$temp = $wp_query121;
												$wp_query121 = null;
												$wp_query121 = new WP_Query($args);
												if( $wp_query121->have_posts() ) { ?>
													<?php while ($wp_query121->have_posts()) : $wp_query121->the_post(); ?>
														<?php if (get_field('new_video')) : ?>
															<iframe width="100%" height="auto" src="https://www.youtube.com/embed/<?php echo the_field('new_video'); ?>?modestbranding=1&autohide=1&showinfo=0&controls=0" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
														<?php else : ?>
														<?php if( get_field('news_img') ):
																	$image = wp_get_attachment_image_src( get_field('news_img'), 'latestnews' );
																	if( !empty($image) ): ?>
																	<a href="<?= $image['0'];?>" rel="lightbox"><img class="img-responsive" src="<?= $image['0'];?>"></a>
														<?php endif; endif; ?>
													<?php endif;?>
														<div class="news">
															<div class="news_bottum">
																<div class="editContent">
																	<p class="lead"><?php the_title();?></p>
																</div>
															</div><!--news_bottum-->
															<div class="news_top">
																<div class="left read_more">
																	<a href="<?php bloginfo('url'); ?>/laatste-nieuws" >Lees Meer <span>>></span></a>
																</div><!--read_more-->
															</div><!--new_top-->
														</div><!--news-->
													<?php endwhile; }  ?>

												</div><!--col-sm-7-->
												<div class="col-md-5 col-xs-12">
													<?php dynamic_sidebar('home_content'); ?>
												</div><!--col-sm-4-->

												<div class="clear"></div>
											</div><!--content_bottum-->

										</div><!--col-sm-9 col-xs-12-->
										<div class="right_sidebar col-sm-4 col-xs-12">
											<?php dynamic_sidebar('home'); ?>
										</div>
										<div class="clear"></div>
									</div><!--inner col-sm-9 col-xs-12-->
									<div class="page_bottom"></div>
									<?php get_footer();?>
