<?php
/**
* Template Name: Diensten
*
*/


get_header(); ?>
<?php if ( get_field( 'hero_fallback', 'option' ) ) : ?>
	<?php $background = get_field( 'hero_fallback', 'option' ); ?>
<?php endif ?>

<?php if ( get_field( 'header_afbeelding', 'option' ) ) : ?>
	<?php $background = get_field('header_afbeelding', 'options'); ?>
<?php endif ?>

<div id="feature" style="background-image: url(<?php echo $background; ?>)">
	<div class="top_shadow"></div>
       <div class="inner col-sm-9 col-xs-12 blog_set container">
         <?php while ( have_posts() ) : the_post(); ?>
         <h1> <?php  the_title();  endwhile;  ?> </h1>

        </div><!--inner col-sm-9 col-xs-12-->
       <div class="bottom_shadow"></div>
   </div><!--feature-->
   <div class="page_top"> </div>
   <!-- Notification block -->
   <?php if ( get_field( 'mededeling_actief', 'option' ) == 1 ) : ?>
	   <div class="container">
		   <div class="vacation-notification__wrapper">
			   <?php if ( get_field( 'mededeling_titel', 'option' ) ) : ?>
				   <div class="vacation-notification__title">
					   <?php the_field( 'mededeling_titel', 'option' ); ?>
				   </div>
			   <?php endif; ?>
			   <?php if ( get_field( 'mededeling_tekst', 'option' ) ) : ?>
				   <div class="vacation-notification__text">
					   <?php the_field( 'mededeling_tekst', 'option' ); ?>
				   </div>
			   <?php endif; ?>
		   </div>
	   </div>
   <?php endif; ?>
   <!-- Notification block -->
    <div class="inner col-sm-9 col-xs-12 container">
        <?php if (function_exists('dimox_breadcrumbs')) dimox_breadcrumbs(); ?>
     <!--col-sm-9 col-xs-12-->
 			 <div class="col-sm-12 col-xs-12 col-md-12 col-lg-12  page_title">
  				<?php the_title(); ?>
 			 </div>
      <div class="content">

             <?php if ( have_rows( 'diensten' ) ) : ?>
             	<?php while ( have_rows( 'diensten' ) ) : the_row(); ?>
                    <div class="col-sm-6 col-xs-12 col-md-4 col-lg-4  posts_contents">
                    <!-- Image -->
                    <?php if ( get_sub_field( 'afbeelding' ) ) : ?>
                        <div class="post_img">
                            <img class="attachment-bosbouw_img size-bosbouw_img wp-post-image" src="<?php the_sub_field( 'afbeelding' ); ?>" />
                        </div>
                    <?php endif ?>
                    <div class="post_ttitle">
                        <a href="<?php the_sub_field( 'link' ); ?>" rel="bookmark">
                            <?php the_sub_field( 'titel' ); ?>
                        </a>
                    </div>
                    <div class="read_more_button"><a href="<?php the_sub_field( 'link' ); ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>">Lees meer &raquo;</a></div>
                    </div>
                <?php endwhile; ?>
             <?php else : ?>
             	<?php // no rows found ?>

             <?php endif; ?>

     <div class="clear"></div>
   </div><!--inner col-sm-9 col-xs-12-->
   <div class="page_bottom"></div>
<?php get_footer();?>
