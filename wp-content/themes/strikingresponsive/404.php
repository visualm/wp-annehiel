<?php get_header(); ?>

<?php if ( get_field( 'hero_fallback', 'option' ) ) : ?>
	<?php $background = get_field( 'hero_fallback', 'option' ); ?>
<?php endif ?>

<?php if ( get_field( 'header_afbeelding', 'option' ) ) : ?>
	<?php $background = get_field('header_afbeelding', 'options'); ?>
<?php endif ?>

<div id="feature" style="background-image: url(<?php echo $background; ?>)">
	<div class="top_shadow"></div>
       <div class="inner col-sm-9 col-xs-12 blog_set container">

         <h1><?php _e( 'Not Found', 'twentyfourteen' ); ?></h1>

        </div><!--inner col-sm-9 col-xs-12-->
       <div class="bottom_shadow"></div>
   </div>
       <div class="page_top"> </div>
	   <!-- Notification block -->
	   <?php if ( get_field( 'mededeling_actief', 'option' ) == 1 ) : ?>
		   <div class="container">
			   <div class="vacation-notification__wrapper">
				   <?php if ( get_field( 'mededeling_titel', 'option' ) ) : ?>
					   <div class="vacation-notification__title">
						   <?php the_field( 'mededeling_titel', 'option' ); ?>
					   </div>
				   <?php endif; ?>
				   <?php if ( get_field( 'mededeling_tekst', 'option' ) ) : ?>
					   <div class="vacation-notification__text">
						   <?php the_field( 'mededeling_tekst', 'option' ); ?>
					   </div>
				   <?php endif; ?>
			   </div>
		   </div>
	   <?php endif; ?>
	   <!-- Notification block -->

	<div class="inner col-sm-9 col-xs-12 container">
			<header class="page-header">
				<h1 class="page-title"><?php _e( 'Not Found', 'twentyfourteen' ); ?></h1>
			</header>

			<div class="page-content">
				<p><?php _e( 'It looks like nothing was found at this location. Maybe try a search?', 'twentyfourteen' ); ?></p>

				<?php get_search_form(); ?>
			</div><!-- .page-content -->
</div><!--inner col-sm-9 col-xs-12-->
    <div class="page_bottom"></div>


<?php
//get_sidebar( 'content' );
//get_sidebar();
get_footer();
