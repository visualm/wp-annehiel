<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme and one
 * of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query,
 * e.g., it puts together the home page when no home.php file exists.
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header(); ?>

<?php if ( get_field( 'hero_fallback', 'option' ) ) : ?>
	<?php $background = get_field( 'hero_fallback', 'option' ); ?>
<?php endif ?>

<?php if ( get_field( 'header_afbeelding', 'option' ) ) : ?>
	<?php $background = get_field('header_afbeelding', 'options'); ?>
<?php endif ?>

<div id="feature" style="background-image: url(<?php echo $background; ?>)">
	<div class="top_shadow"></div>
       <div class="inner col-sm-9 col-xs-12 blog_set container">
           <h1>Blog</h1>

        </div><!--inner col-sm-9 col-xs-12-->
       <div class="bottom_shadow"></div>
   </div>

   <div class="page_top"> </div>
   <!-- Notification block -->
   <?php if ( get_field( 'mededeling_actief', 'option' ) == 1 ) : ?>
	   <div class="container">
		   <div class="vacation-notification__wrapper">
			   <?php if ( get_field( 'mededeling_titel', 'option' ) ) : ?>
				   <div class="vacation-notification__title">
					   <?php the_field( 'mededeling_titel', 'option' ); ?>
				   </div>
			   <?php endif; ?>
			   <?php if ( get_field( 'mededeling_tekst', 'option' ) ) : ?>
				   <div class="vacation-notification__text">
					   <?php the_field( 'mededeling_tekst', 'option' ); ?>
				   </div>
			   <?php endif; ?>
		   </div>
	   </div>
   <?php endif; ?>
   <!-- Notification block -->


<!-- <div class="woo_prroduct_outer">
  <div class="static_result">
    <div class="woo_prroductdate col-sm-2 formobflot ">Datum</div>
    <div class="woo_prroducttime col-sm-2 formobflot ">Tijd</div>
    <div class="woo_prroduct_des col-sm-6 formobflot">Onderdeel</div>
    <div class="woo_prroductlink col-sm-2 formobflot"></div>

  </div>


</div> -->
		<?php

			 $args = array('post_type' => 'post');

// The Query
$the_query = new WP_Query( $args );

			if ( $the_query->have_posts() ) :
				// Start the Loop.
				 while ($the_query->have_posts()) : $the_query->the_post();

					/*
					 * Include the post format-specific template for the content. If you want to
					 * use this in a child theme, then include a file called called content-___.php
					 * (where ___ is the post format) and that will be used instead.
					 */
					//get_template_part( 'content', get_post_format() ); ?>



	<!-- Start Basic 1-7 -->
	<section id="basic-1-7" class="basic-1-7 content-block">
		<div class="container">
			<div class="row">

				<div class="col-lg-4 col-md-5 col-sm-12">
					<?php if (get_field('new_video')) : ?>
						<iframe width="100%" height="auto" src="https://www.youtube.com/embed/<?php echo the_field('new_video'); ?>" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
					<?php else : ?>
					<?php if( get_field('news_img') ):
								$image = wp_get_attachment_image_src( get_field('news_img'), 'latestnews' );
								if( !empty($image) ): ?>
								<a href="<?= $image['0'];?>" rel="lightbox"><img class="img-responsive" src="<?= $image['0'];?>"></a>
					<?php endif; endif; ?>
				<?php endif;?>
					<br /><br />
				</div>

				<div class="col-lg-8 col-md-7 col-sm-12">
					<div class="editContent">
						<h2><?php  the_title();?></h2>
					</div>
					<div class="editContent">
						<?php if (get_field('news_date')) : ?>
							<p><b>Periode:</b> <?php echo the_field('news_date'); ?></p>
						<?php endif; ?>
						<?php if (get_field('omschrijving')) : ?>
							<p><?php the_field('omschrijving'); ?></p>
						<?php endif; ?>
					</div>
				</div>

			</div><!-- /.row -->
		</div><!-- /.container -->
	</section>
    <!--// END Basic 1-7 -->




</div>
<?php
				endwhile;
				// Previous/next post navigation.
				//twentyfourteen_paging_nav();

			else :
				// If no content, include the "No posts found" template.
				get_template_part( 'content', 'none' );

			endif;
		?>

	<!-- #content -->
	<!-- #primary -->
	<?php //get_sidebar( 'content' ); ?>
<!-- #main-content -->
</div>
   <div class="page_bottom"></div>
<?php
//get_sidebar();
get_footer();
