<?php
/**
 * Template Name:Bosbouw
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header(); ?>

<?php if ( get_field( 'hero_fallback', 'option' ) ) : ?>
	<?php $background = get_field( 'hero_fallback', 'option' ); ?>
<?php endif ?>

<?php if ( get_field( 'header_afbeelding', 'option' ) ) : ?>
	<?php $background = get_field('header_afbeelding', 'options'); ?>
<?php endif ?>

<div id="feature" style="background-image: url(<?php echo $background; ?>)">

	<div class="top_shadow"></div>
       <div class="inner col-sm-9 col-xs-12 blog_set container">

       <?php $slidinghide= get_post_meta($post->ID, 'my_meta_box_check', true);?>
       <?php if($slidinghide=='on') {

	  ?>

     <h1><?php echo $parent_title = get_the_title($post->post_parent); ?></h1>
         <?php  if(!$post->post_parent==0) { ?>
      <h3>'<?php the_title(); ?>'</h3> <?php } ?>
	 <?php  }  else { ?>




       <?php  $slider_code_page=get_post_meta($post->ID, 'slider_code_page', true);
      if($slider_code_page) { ?>
      <?php echo  do_shortcode($slider_code_page); } else
	  { ?>

        <?php echo do_shortcode('[cycloneslider id="bosbouw-slider"]');
		 } ?>
       <?php } ?>

        </div><!--inner col-sm-9 col-xs-12-->
       <div class="bottom_shadow"></div>
   </div><!--feature-->

   <div class="page_top">


   </div><!--page_top-->
   <!-- Notification block -->
   <?php if ( get_field( 'mededeling_actief', 'option' ) == 1 ) : ?>
	   <div class="container">
		   <div class="vacation-notification__wrapper">
			   <?php if ( get_field( 'mededeling_titel', 'option' ) ) : ?>
				   <div class="vacation-notification__title">
					   <?php the_field( 'mededeling_titel', 'option' ); ?>
				   </div>
			   <?php endif; ?>
			   <?php if ( get_field( 'mededeling_tekst', 'option' ) ) : ?>
				   <div class="vacation-notification__text">
					   <?php the_field( 'mededeling_tekst', 'option' ); ?>
				   </div>
			   <?php endif; ?>
		   </div>
	   </div>
   <?php endif; ?>
   <!-- Notification block -->
   <div class="inner col-sm-9 col-xs-12 col-lg-9 col-md-9 container">
    <?php if (function_exists('dimox_breadcrumbs')) dimox_breadcrumbs(); ?>
     <!--col-sm-9 col-xs-12-->
					<?php
					$paged = get_query_var('paged') ? get_query_var('paged') : 1;
        $args=array(
          'post_type' => 'bosbouw',
          'post_status' => 'publish',
          'posts_per_page' => 9,'paged' => $paged,
		  'orderby'   => 'post_title',
		  'order' => 'ASC',
        );
          ?>
 			 <div class="col-sm-12 col-xs-12 col-md-12 col-lg-12  page_title">
  				<?php the_title(); ?>
 			 </div>
			<?php

            $my_query = new WP_Query($args);
            if( $my_query->have_posts() ) {
              while ($my_query->have_posts()) : $my_query->the_post(); ?>
  			<div class="col-sm-6 col-xs-12 col-md-4 col-lg-4  posts_contents">
    			<div class="post_img">
   					<a href="<?php the_permalink(); ?>" rel="bookmark"> <?php the_post_thumbnail('bosbouw_img');?></a>
   				 </div>
   			 <div class="post_ttitle">
    				<a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a>
    		</div>
    		<div class="read_more_button"><a href="<?php the_permalink(); ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>">Lees meer &raquo;</a></div>
    </div>
    <?php
  endwhile;
}

  wp_pagenavi( array( 'query' => $my_query ) );

wp_reset_query();  // Restore global post data stomped by the_post().
?>

     <div class="clear"></div>
   </div><!--inner col-sm-9 col-xs-12-->
   <div class="page_bottom"></div>
<?php get_footer();?>
