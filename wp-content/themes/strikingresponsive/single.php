<?php
/**
 * The Template for displaying all single posts
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header(); ?>
<?php if ( get_field( 'hero_fallback', 'option' ) ) : ?>
	<?php $background = get_field( 'hero_fallback', 'option' ); ?>
<?php endif ?>

<?php if ( get_field( 'header_afbeelding', 'option' ) ) : ?>
	<?php $background = get_field('header_afbeelding', 'options'); ?>
<?php endif ?>

<div id="feature" style="background-image: url(<?php echo $background; ?>)">
	<div class="top_shadow"></div>
       <div class="inner col-sm-9 col-xs-12 blog_set container">
         <?php while ( have_posts() ) : the_post(); ?>
         <h1> <?php  the_title();  endwhile;  ?> </h1>

        </div><!--inner col-sm-9 col-xs-12-->
       <div class="bottom_shadow"></div>
   </div><!--feature-->
   <div class="page_top"> </div>
   <!-- Notification block -->
   <?php if ( get_field( 'mededeling_actief', 'option' ) == 1 ) : ?>
	   <div class="container">
		   <div class="vacation-notification__wrapper">
			   <?php if ( get_field( 'mededeling_titel', 'option' ) ) : ?>
				   <div class="vacation-notification__title">
					   <?php the_field( 'mededeling_titel', 'option' ); ?>
				   </div>
			   <?php endif; ?>
			   <?php if ( get_field( 'mededeling_tekst', 'option' ) ) : ?>
				   <div class="vacation-notification__text">
					   <?php the_field( 'mededeling_tekst', 'option' ); ?>
				   </div>
			   <?php endif; ?>
		   </div>
	   </div>
   <?php endif; ?>
   <!-- Notification block -->
    <div class="inner col-sm-9 col-xs-12 container">
    <?php if (function_exists('dimox_breadcrumbs')) dimox_breadcrumbs(); ?>
      <div class="content">

     <?php while ( have_posts() ) : the_post(); ?>
      <h2><?php the_title(); ?></h2>
      <?php    $postid = get_the_ID();     ?>
 		<!-- <div class="date_onsingel"><h3 class="col-xs-3 fnone">Datum: <span><?php echo get_post_meta($post->ID,'my_meta_box_text_date',true); ?></span></h3></div> -->

        <!-- <div class="date_onsingel"><h3 class="col-xs-3 fnone">Tijd: <span><?php echo get_post_meta($post->ID,'my_meta_box_text_time',true); ?></span> </h3></div> -->



	 <?php  the_content(); endwhile;  ?>

       </div><!--content-->
    </div><!--inner col-sm-9 col-xs-12-->
    <div class="page_bottom"></div>
<?php
//get_sidebar();
get_footer();
