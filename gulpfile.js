//******************************************************************************
//******************************************************************************
//******************************************************************************
// Name of theme
var theme 					= 'annehiel';
var domain 					= 'annehiel';
var foldername				= 'annehiel.nl';

//******************************************************************************
//******************************************************************************
//******************************************************************************
// Settings
var gulp 					= require('gulp');
var browserSync 			= require('browser-sync').create();
var notify 					= require("gulp-notify");

// Compiling
var rename 					= require("gulp-rename");
var plumber 				= require('gulp-plumber');
var pump 					= require('pump');
var uglify 					= require('gulp-uglify');

// SASS/CSS
var sass 					= require('gulp-sass');
var criticalCss 			= require('gulp-critical-css');
var sourcemaps 				= require("strip-source-map-loader");

// Deploy
var rsync 					= require("gulp-rsync");
var argv   					= require('minimist')(process.argv);
var gulpif 					= require('gulp-if');
var prompt 					= require('gulp-prompt');
var inquirer				= require('inquirer');

//Images
var imagemin				= require('gulp-imagemin');

//init
var decompress 				= require('gulp-decompress');
var download   				= require('gulp-download');
var del        				= require('del');
var open       				= require('gulp-open');
var run        				= require('gulp-run');
var git 					= require('gulp-git');
var replace 				= require('gulp-replace');
var createDbManager 		= require('manage-database');
var wait 					= require('gulp-wait');

//Database settings
var staging_db = {
	name : "vm_" + domain,
	user : "annehiel",
	password : "Ux5E8Ka0ju4M8dmC4thbxoaa",
	host : "149.210.197.75",
	prefix : "vm_",
};

var staging = {
	hostname : "149.210.197.75",
	username :'annehiel',
	destination : '/home/' + domain + '/',
	root : '/',
};

var production = {
	hostname : "35.204.119.150",
	username :"",
	destination : "",
	root : "/",
};

// Paths
var paths = {
	maindest: 				'',
	scss: 					'wp-content/themes/' + theme + '/scss/style.scss',
	main: 					'wp-content/themes/' + theme + '/js/main/main.js',
	js: 					'wp-content/themes/' + theme + '/js/*',
	img: 					'wp-content/themes/' + theme + '/img/*',
	plugins: 				'wp-content/themes/' + theme + '/js/plugins/*.js',
	dist_js: 				'wp-content/themes/' + theme + '/dist/js',
	dist_css: 				'wp-content/themes/' + theme + '/dist/css',
	dist_img: 				'wp-content/themes/' + theme + '/dist/img',
	theme_path: 			'wp-content/themes/' + theme,
	build: 					'/',
};

//******************************************************************************
//******************************************************************************
//******************************************************************************
// Helper tasks

//******************************************************************************
// Gives a message to messagecenter on error
var onError = function(err) {
	notify({
		title: 'Error',
		message: err.message
	}).write(err);

	console.log(err.toString());
};

//******************************************************************************
// Task for compiling CSS
gulp.task('sass', function(cb) {
	pump([
			gulp.src(paths.scss),
			sass({ outputStyle: 'compressed' }).on('error', onError),
			gulp.dest(paths.dist_css),
			browserSync.stream({ match: '**/*.css' })
		],
		cb
	);
});

//******************************************************************************
// Task for compiling critical CSS
gulp.task('critical', function(cb) {
	pump([
			gulp.src(paths.dist_css + '/style.css'),
			criticalCss(),
			gulp.dest(paths.dist_css),
		],
		cb
	);
});

//******************************************************************************
// Task for creating sourcemaps
gulp.task('sourcemaps', function(cb) {
	pump([
			gulp.src(paths.js),
			sourcemaps.init({ loadMaps: true }),
			sourcemaps.write('/dev/null', { addComment: false }),
			gulp.dest(paths.dist_js)
		],
		cb
	);
});

//******************************************************************************
// Task for compiling main.js
gulp.task('js-main', function(cb) {
	pump([
			gulp.src(paths.main),
			rename({ extname: '.min.js' }),
			uglify().on('error', onError),
			gulp.dest(paths.dist_js),
		],
		cb
	);
});

//******************************************************************************
// Task for compiling the plugins *.js
gulp.task('js-plugins', function(cb) {
	pump([
			gulp.src(paths.plugins),
			rename({ extname: '.min.js' }),
			uglify().on('error', onError),
			gulp.dest(paths.dist_js),
		],
		cb
	);
});

//******************************************************************************
// Task for minifiing images
gulp.task('images', function(cb) {
	pump([
			gulp.src(paths.img),
			imagemin([
				imagemin.gifsicle({interlaced: true}),
				imagemin.jpegtran({progressive: true}),
				imagemin.optipng({optimizationLevel: 5}),
				imagemin.svgo({
					plugins: [
						{removeViewBox: true},
						{cleanupIDs: true}
					]
				})
			], { verbose: true }).on('error', onError),
			gulp.dest(paths.dist_img),
		],
		cb
	);
});


//******************************************************************************
// General bowser-sync task
gulp.task('browser-sync', function() {
	browserSync.init(["**/*.css", "**/*.js", '**/*.html', '**/*.php', '**/*.scss'], {
		proxy: 'localhost:8888/wordpress/' + foldername + '/'
	});
});

//******************************************************************************
//******************************************************************************
//******************************************************************************
// Gulp task for install

// Download Wordpress / init
gulp.task('wp-download', function () {
	return download('https://wordpress.org/latest.tar.gz')
		.pipe(gulp.dest("./downloads"));
});

// Unzip Wordpress
gulp.task('unzip-wp', ['wp-download'], function () {
	return gulp.src('downloads/latest.tar.gz')
		.pipe(decompress({strip: 1}))
		.pipe(gulp.dest('./'));
});

// Delete default plugins
gulp.task('delete-default-plugins',['unzip-wp'], function(){
	return del(['wp-content/plugins/**']);
});

// Delete default Wordpress themes
gulp.task('delete-default-themes',['delete-default-plugins'], function(){
	return del(['wp-content/themes/**']);
});

// Delete Wordpress unused files
gulp.task('delete-wp-unused-files',['delete-default-themes'], function(){
	return del(['license.txt', 'readme.html']);
});

// Install plugins from repos
gulp.task('install-plugins', ['delete-wp-unused-files'], function () {
	git.clone('https://visualmedianl@bitbucket.org/visualm/plugin-builder.git', {args: 'wp-content/plugins/vm-builder-plugin'}, function (err) {
		if (err) throw err;
	});
});

// Download Yoast
gulp.task('Yoast', ['install-plugins'], function () {
	return download('http://downloads.wordpress.org/plugin/wordpress-seo.latest-stable.zip')
		.pipe(gulp.dest("./downloads"));
});

// Unzip Yoast
gulp.task('unzip-yoast', ['Yoast'], function () {
	return gulp.src('downloads/wordpress-seo.latest-stable.zip')
		.pipe(decompress({strip: 1}))
		.pipe(gulp.dest('./wp-content/plugins/wordpress-seo'));
});

// Download Theme Code Pro
gulp.task('themecodepro', ['unzip-yoast'], function () {
	return download('https://hookturn.io/index.php?eddfile=5934%3A15%3A2%3A0&ttl=1521939080&file=2&token=7c194123b7b6286fe03d756347c2ceeb')
		.pipe(rename("themecodepro.zip"))
		.pipe(gulp.dest("./downloads"));
});

// Unzip Theme Code Pro
gulp.task('unzip-themecodepro', ['themecodepro'], function () {
	return gulp.src('downloads/themecodepro.zip')
		.pipe(decompress({strip: 1}))
		.pipe(gulp.dest('./wp-content/plugins/theme-code-pro'));
});

// Download ACF
gulp.task('ACF', ['unzip-themecodepro'], function () {
	return download('https://connect.advancedcustomfields.com/index.php?a=download&p=pro&k=b3JkZXJfaWQ9NDE2OTJ8dHlwZT1kZXZlbG9wZXJ8ZGF0ZT0yMDE0LTEwLTA5IDExOjU0OjIy')
		.pipe(rename("acf.zip"))
		.pipe(gulp.dest("./downloads"));
});

// Unzip ACF
gulp.task('unzip-acf', ['ACF'], function () {
	return gulp.src('downloads/acf.zip')
		.pipe(decompress({strip: 1}))
		.pipe(gulp.dest('./wp-content/plugins/advanced-custom-field'));
});

// Unzip Starter theme to Wordpress
gulp.task('install-theme-basic', ['unzip-acf'], function () {
	git.clone('https://visualmedianl@bitbucket.org/visualm/theme-basic.git', {args: paths.theme_path}, function (err) {
		if (err) throw err;
		gulp.start('delete-files');
	});
});

// Unzip Starter theme to Wordpress
gulp.task('install-theme-core', ['unzip-acf'], function () {
	git.clone('https://visualmedianl@bitbucket.org/visualm/theme-core.git', {args: paths.theme_path}, function (err) {
		if (err) throw err;
		gulp.start('delete-files');
	});
});

// edit wp config
gulp.task('wp-config', ['install-theme-core'], function(){
	gulp.src(['./wp-config-sample.php'])
		.pipe(replace("define('DB_NAME', 'database_name_here');", "define('DB_NAME', '" + staging_db.name + "');"))
		.pipe(replace("define('DB_USER', 'username_here');", "define('DB_USER', '" + staging_db.user + "');"))
		.pipe(replace("define('DB_PASSWORD', 'password_here');", "define('DB_PASSWORD', '" + staging_db.password + "');"))
		.pipe(replace("define('DB_HOST', 'localhost');", "define('DB_HOST', '" + staging_db.host + "');"))
		.pipe(replace("$table_prefix  = 'wp_';", "$table_prefix  = '" + staging_db.prefix + "';"))
		.pipe(replace("define('WP_DEBUG', false);", "define('WP_DEBUG', true);"))
		.pipe(rename("/wp-config.php"))
		.pipe(gulp.dest('./'));
});

gulp.task('create-db', ['wp-config'], function(){
	var dbManager = createDbManager({
		user: staging_db.user,
		password: staging_db.password,
		host: staging_db.host,
	});

	// create database
	dbManager.create(staging_db.name, function (err) {
		if (err) {
			console.error(err);
			return;
		}
		console.log('database created');
	});
});

gulp.task('start-wp', ['create-db'], function () {
	// gulp.start('wp');
});

gulp.task('delete-files' , function () {
	return del(['downloads/**']);
});

gulp.task('install', function () {
	var questions = [{
		type: 'confirm',
		name: 'yousure',
		message: 'Are you sure you want to install?',
		default: false
	}];

	if (argv.core){
		return inquirer.prompt(questions).then(function(answers) {
			if (!answers.yousure) {
				return process.exit (2);
			}
			gulp.start('start-wp');
		});

	} else if (argv.basic){
		return inquirer.prompt(questions).then(function(answers) {
			if (!answers.yousure) {
				return process.exit (2);
			}
			gulp.start('start-wp');
		});

	} else {
		onError('No theme defined');
		return false;
	}
});

//******************************************************************************
//******************************************************************************
//******************************************************************************
// Gulp task for deployment
gulp.task('deploy', function() {

	// Dirs and Files to sync
	rsyncPaths = [paths.build];

	// Default options for rsync
	rsyncConf = {
		progress: true,
		incremental: true,
		relative: true,
		emptyDirectories: true,
		recursive: true,
		archive: true,
		update: true,
		clean: false,
		command: true,
		verbose: true,
		exclude: [
			paths.js,
			paths.css,
		],
	};

	// Staging
	if (argv.staging) {
		rsyncConf.hostname 			= staging.hostname;
		rsyncConf.username 			= staging.username;
		rsyncConf.destination 		= staging.destination;
		rsyncConf.root				= staging.root;

		return gulp.src(rsyncPaths)
			.pipe(gulpif(
				argv.staging,
				prompt.confirm({
					message: 'Heads Up! Are you SURE you want to push to STAGING?',
					default: false
				})
			))
			.pipe(rsync(rsyncConf));

	} else if(argv.production) {
		onError('Production not configured yet!');
		return false;
	} else {
		onError('No server defined');
		return false;
	}
});

//******************************************************************************
//******************************************************************************
//******************************************************************************
// Gulp tasks we can run

//******************************************************************************
// Gulp task for wordpress
gulp.task('wp', ['browser-sync'], function() {
	gulp.start('sass');
	gulp.start('critical');
	gulp.start('js-main');
	gulp.start('js-plugins');
	gulp.start('images');

	// CSS
	gulp.watch("wp-content/themes/*/scss/**/*.scss", ['sass']);
	gulp.watch("wp-content/themes/*/dist/css/style.css", ['critical']);

	// JS
	gulp.watch('wp-content/themes/*/js/main/*.js', ['js-main']);
	gulp.watch('wp-content/themes/*/js/plugins/!(*.min.js)', ['js-plugins']);

	// images
	gulp.watch('wp-content/themes/*/img/*.png', ['images']);
	gulp.watch('wp-content/themes/*/img/*.jpg', ['images']);
	gulp.watch('wp-content/themes/*/img/*.jpeg', ['images']);
	gulp.watch('wp-content/themes/*/img/*.gif', ['images']);
	gulp.watch('wp-content/themes/*/img/*.svg', ['images']);

	// BrowserSync
	gulp.watch("wp-content/themes/*/*.html").on('change', browserSync.reload).on('error', onError);
	gulp.watch("wp-content/themes/*/*.php").on('change', browserSync.reload).on('error', onError);
});

//******************************************************************************
// Gulp task for plain html
gulp.task('default', ['browser-sync'], function() {
	gulp.watch("**/*.scss", ['sass']);
	gulp.watch('js/main/*.js', ['js-main']);
	gulp.watch('js/plugins/*.js', ['js-main']);
	gulp.watch("*.html").on('change', browserSync.reload).on('error', onError);
	gulp.watch("*.php").on('change', browserSync.reload).on('error', onError);
});